export default function awesomeEmojiLog(message) {
    if (message === undefined) throw new Error("No Message Found");
    console.log("😎", message)
};

awesomeEmojiLog("This is awesome emoji")